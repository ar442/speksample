package xyz.qwexter.speksample.utils

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.spekframework.spek2.lifecycle.LifecycleListener
import org.spekframework.spek2.lifecycle.TestScope

class SpekRxSchedulers : LifecycleListener {

    val testScheduler = TestScheduler()

    override fun beforeExecuteTest(test: TestScope) {
        RxJavaPlugins.setInitIoSchedulerHandler { testScheduler }
        RxJavaPlugins.setInitComputationSchedulerHandler { testScheduler}
        RxJavaPlugins.setInitNewThreadSchedulerHandler { testScheduler }

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { testScheduler }
    }

    override fun afterExecuteTest(test: TestScope) {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

    fun triggerAll() {
        testScheduler.triggerActions()
    }

}