package xyz.qwexter.speksample

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

object SpekExampleUnitCucumberTest : Spek({
    // Это блок описания фичи, который содержит в себе сценарии использования ее
    Feature("Пример теста в стиле Cucumber") {

        val set by memoized { mutableSetOf<String>() }

        Scenario("X+2 = 4 и добавили в Set") {
            When("Операция добавления") {
                set.add((2 + 2).toString())
            }
            Then("Проверяем что в коллекии 1 элемент") {
                assertEquals(1, set.size)
            }
            Then("Проверяем что в коллекии содержитсья результат сложения") {
                assertTrue(set.contains("4"))
            }
        }
    }
})

