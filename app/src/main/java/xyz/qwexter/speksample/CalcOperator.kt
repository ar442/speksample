package xyz.qwexter.speksample

import io.reactivex.Single

interface CalcOperator {
    fun calc(left: Int, right: Int) : Single<Int>
}