package xyz.qwexter.speksample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainView {

    lateinit var presenter: MainPresenter

    private val operator = AddOperator()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lifecycle.addObserver(presenter)
        presenter = MainPresenter(this, operator)
        activity_main_calc.setOnClickListener {
            presenter.calc(
                activity_main_input_1.text.toString(),
                activity_main_input_2.text.toString()
            )
        }
    }

    override fun onDestroy() {
        lifecycle.removeObserver(presenter)
        super.onDestroy()
    }

    override fun showResult(result: Int) {
        activity_main_result.text = getString(R.string.result_formatter, result)
    }

}
