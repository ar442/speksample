package xyz.qwexter.speksample


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        val inputOneView = onView(
            allOf(
                withId(R.id.activity_main_input_1),
                isDisplayed()
            )
        )
        inputOneView.perform(replaceText("1"), closeSoftKeyboard())

        val inputTwoView = onView(
            allOf(
                withId(R.id.activity_main_input_2),
                isDisplayed()
            )
        )
        inputTwoView.perform(replaceText("2"), closeSoftKeyboard())
        val buttonView = onView(
            allOf(
                withId(R.id.activity_main_calc),
                isDisplayed()
            )
        )
        buttonView.perform(click())
        onView(withId(R.id.activity_main_result)).check { view, _ ->
            assertThat(view, withText("Result: 3"))
        }

    }
}
