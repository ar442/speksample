# speksample

Sample project for spek 2.0

# Подключение
 Spek2 работает на основе JUnit5, т.к. Android Gradle Plugin не имеет встроенной 
 поддежки, необходимо подключить внешний плагин. Для этого:
 * в `build.gradle` проекта необзодима добавить зависимость:
   ```
    classpath "de.mannodermaus.gradle.plugins:android-junit5:1.3.2.0"
    ```
 * обновить версию gradle до 4.7 и выше;
 * настройить JUnit5 плагин в `build.gradle` модуля:
    ```
    plugins {
        // For run JUnit5 test
        id "de.mannodermaus.android-junit5"
    }
    ...
    android {
        ...
        defaultConfig {
            ...
            testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
            testInstrumentationRunnerArgument "de.mannodermaus.junit5.AndroidJUnit5Builder"
        }
        ...
        testOptions {
            junitPlatform {
                filters {
                    engines {
                        include 'spek'
                    }
                }
            }
        }
    }
    ...
    dependencies {
        ...
        // для запуска на базе JUnit5 тестов
        testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.3.2'
        
        // для запуска инструментальных тестов на JUnit5
        androidTestImplementation 'de.mannodermaus.junit5:android-instrumentation-test-runner:0.2.2'
        androidTestRuntimeOnly "org.junit.jupiter:junit-jupiter-engine:5.3.2"
        androidTestRuntimeOnly "org.junit.platform:junit-platform-runner:1.3.2"
        androidTestRuntimeOnly "de.mannodermaus.junit5:android-instrumentation-test-runner:0.2.2"
        
        // для корректного запуска junit 4 тестов на JUnit5
        testImplementation 'junit:junit:4.12'
        testRuntimeOnly 'org.junit.vintage:junit-vintage-engine:5.3.1'
        
        // для запуска параметризированных тестов
        testImplementation 'org.junit.jupiter:junit-jupiter-params:5.3.2'
        testImplementation 'org.mockito:mockito-core:2.23.0'
        testImplementation 'org.robolectric:robolectric:3.7.1'
        }
    ```
* настроить сам Spek, в `build.gradle` модуля:
    ```
     android {
            ...
            defaultConfig {
                ...
                // add runnerBuilder
                testInstrumentationRunnerArgument "runnerBuilder", "de.mannodermaus.junit5.AndroidJUnit5Builder"
            }
            ...
                testOptions {
            junitPlatform {
                filters {
                    engines {
                        include 'spek'
                    }
                }
            }
        }
    ...
    dependencies {
        ...
        // spek2
        testImplementation 'org.spekframework.spek2:spek-dsl-jvm:2.0.0-rc.1'
        testImplementation 'org.spekframework.spek2:spek-runner-junit5:2.0.0-rc.1'
        testImplementation 'org.jetbrains.kotlin:kotlin-reflect:1.3.11'
        ...
    }
    ```
* установить плагин для Android Studio, иначе нельзя будет из GUI запускать тесты

# Особенности использования
Spek2 имеет возможность работать с 2 разными стилями описания - [Specification](https://spekframework.org/specification/) 
и [Gherkin](https://spekframework.org/gherkin/).
Основная идея спецификаий -> все лямбда. Каждый блок или тест - лямбда, удобно 
описывать параметризированные тесты:
```
    describe("Проверяем позитивные сценарии сложения аргументов") {
            listOf(
                Triple(0, 0, 0),
                Triple(1, 0, 1),
                Triple(0, 1, 1),
                Triple(100, 1, 101),
                Triple(100, -1, 99)
            ).forEach { (first, second, result) ->
                it("Вызывается showResult c значением равным: $first + $second = $result") {
                    presenter.calc(first.toString(), second.toString())
                    Mockito.verify(view).showResult(eq(result))
                    Mockito.verifyNoMoreInteractions(view)
                }
            }
        }
```
Из коробки нет возможности пользоваться `@Rules` из `JUnit4`, но есть порт, который 
может частично помочь решить эту проблему [spek-junit-rules](https://github.com/Ullink/spek-junit-rules).
В случае необхдимости возможно описать самостоятельно аналог правил с помощью 
`LifecycleListener`, например аналаг правила для тестирования RxJava2:
```
class SpekRxSchedulers : LifecycleListener {

    val testScheduler = TestScheduler()

    override fun beforeExecuteTest(test: TestScope) {
        RxJavaPlugins.setInitIoSchedulerHandler { testScheduler }
        RxJavaPlugins.setInitComputationSchedulerHandler { testScheduler}
        RxJavaPlugins.setInitNewThreadSchedulerHandler { testScheduler }

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { testScheduler }
    }

    override fun afterExecuteTest(test: TestScope) {
        RxAndroidPlugins.reset()
        RxJavaPlugins.reset()
    }

    fun triggerAll() {
        testScheduler.triggerActions()
    }

}
```
Для того чтобы воспользоваться им надо его добавить в корень теста:
```
class MainPresenterTest : Spek({

    val rxLifecycleListener = SpekRxSchedulers()

    registerListener(rxLifecycleListener)
...
}
```
# Проблемы
На данный момент нет возможности запускать инструменталные и интеграционные тесты
через spek testRunner. Возможно, в скором времени удасться запустить Robolectric 
под spek, но это не точно.
Так же на момент написания не удалось решить проблему с запуском jacoco, на
последних версиях Gradle (4.8, 4.9, 5.1), из-за конфликта зависимотсей плагинов
JaCoCo в встроенном в Gradle и в Android-JUnit5.
